### Каркас для быстрой разработки, мультиязычного сайта на реакте


![](./img.jpg "")

#### Технологии
* react
* react-dom

#### Запуск
1. npm i -D
1. npm start


#### Получить помощь
https://bitbucket.org/stasok/multi-language/pull-requests/ - Ваши пожелания и замечания к проекту.


